export class Reservacion{
    constructor(
        public id: number,
        public lugarId: number,
        public usuarioId: number,
        public lugaresTitulo: string,
        public huespedes: number,
    ){}
}